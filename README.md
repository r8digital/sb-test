# SbUsers

This is a demo project for nickspence.com.

A production build can be viewed at: [https://nickspence.com/sb-test/](https://nickspence.com/sb-test/).

Code coverage report: [https://nickspence.com/sb-test/coverage](https://nickspence.com/sb-test/coverage/).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

