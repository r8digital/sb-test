import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get<User[]>('https://uitest.free.beeceptor.com/usernames');
  }
}
