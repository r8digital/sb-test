import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UserServiceService } from './user-service.service';
import { User } from '../models/user.interface';

describe('UserServiceService', () => {
  let httpMock: HttpTestingController;
  let service: UserServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ UserServiceService ]
    });

    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(UserServiceService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should return array of users', () => {
    const testUsers: User[] = [
      { name: 'Nick' },
      { name: 'Tom' }
    ];

    service.getUsers().subscribe(data => {
        expect(data.length).toBe(2);
        expect(data).toEqual(testUsers);
    });

    const request = httpMock.expectOne('https://uitest.free.beeceptor.com/usernames');
    expect(request.request.method).toBe('GET');
    request.flush(testUsers);
  });
});
