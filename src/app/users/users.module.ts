import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { UsersRoutingModule } from './users-routing.module';
import { reducer } from './state/reducer';
import { UsersEffects } from './state/effects';

import { UsersComponent } from './containers/users.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { UserComponent } from './components/user/user.component';
import { ApiFailureComponent } from './components/api-failure/api-failure.component';


@NgModule({
  declarations: [UsersComponent, AddUserComponent, ListUsersComponent, UserComponent, ApiFailureComponent],
  imports: [
    CommonModule,
    EffectsModule.forRoot([UsersEffects]),
    ReactiveFormsModule,
    StoreModule.forFeature('users-module', reducer),
    UsersRoutingModule
  ]
})
export class UsersModule { }
