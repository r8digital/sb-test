import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { LoadUsersAction, EditUserAction, DeleteUserAction, AddUserAction } from '../state/actions';
import { selectUsers, selectUsersFailure } from '../state/selectors';

import { User } from '../models/user.interface';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]>;
  usersApiFailure$: Observable<boolean>;

  constructor(private store: Store<any>) {}

  ngOnInit() {
    this.loadUsers();
    this.setupSubscriptions();
  }

  onEdit(user: User) {
    this.store.dispatch(new EditUserAction(user));
  }

  onDelete(index: number) {
    this.store.dispatch(new DeleteUserAction(index));
  }

  onAddUser(user: User) {
    this.store.dispatch(new AddUserAction(user));
  }

  loadUsers() {
    this.store.dispatch(new LoadUsersAction());
  }

  setupSubscriptions() {
    this.users$ = this.store.pipe(select(selectUsers));
    this.usersApiFailure$ = this.store.pipe(select(selectUsersFailure));
  }
}
