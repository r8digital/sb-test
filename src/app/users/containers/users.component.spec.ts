import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule, Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AddUserComponent } from '../components/add-user/add-user.component';
import { ApiFailureComponent } from '../components/api-failure/api-failure.component';
import { ListUsersComponent } from '../components/list-users/list-users.component';
import { UserComponent } from '../components/user/user.component';
import { UsersComponent } from './users.component';
import { selectUsers, selectUsersFailure } from '../state/selectors';
import { AddUserAction, DeleteUserAction, EditUserAction } from '../state/actions';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let store: MockStore<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        AddUserComponent,
        ApiFailureComponent,
        ListUsersComponent,
        UserComponent,
        UsersComponent
      ],
      providers: [
        provideMockStore({})
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should call loadUsers() and setupSubscriptions() onInit', () => {
    const spySetupSubs = spyOn(component, 'setupSubscriptions');
    const spyLoadUsers = spyOn(component, 'loadUsers');

    component.ngOnInit();

    expect(spySetupSubs).toHaveBeenCalled();
    expect(spyLoadUsers).toHaveBeenCalled();
  });

  it('should setup subscription to users', () => {
    const users = [
      { name: 'Nick' },
      { name: 'Tom' }
    ];
    store.overrideSelector(selectUsers, users);

    component.setupSubscriptions();

    let results = null;
    component.users$.subscribe(data => results = data);

    expect(results).toEqual(users);
  });

  it('should setup subscription to api failure', () => {
    store.overrideSelector(selectUsersFailure, false);

    component.setupSubscriptions();

    let results = null;
    component.usersApiFailure$.subscribe(fail => results = fail);

    expect(results).toEqual(false);
  });

  it('should dispatch AddUserAction', () => {
    const user = { name: 'Nick' };
    const action = new AddUserAction(user);
    const spy = spyOn(store, 'dispatch');

    component.onAddUser(user);

    expect(spy).toHaveBeenCalledWith(action);
  });

  it('should dispatch DeleteUserAction', () => {
    const action = new DeleteUserAction(1);
    const spy = spyOn(store, 'dispatch');

    component.onDelete(1);

    expect(spy).toHaveBeenCalledWith(action);
  });

  it('should dispatch DeleteUserAction', () => {
    const user = { name: 'Nick', index: 4 };
    const action = new EditUserAction(user);
    const spy = spyOn(store, 'dispatch');

    component.onEdit(user);

    expect(spy).toHaveBeenCalledWith(action);
  });

});
