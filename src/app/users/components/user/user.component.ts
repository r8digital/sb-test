import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/user.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  @Input() user: User;
  @Input() index: number;
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();

  form: FormGroup;
  editMode = false;

  get name() { return this.form.get('name'); }

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.buildForm();
  }

  toggleMode() {
    this.editMode = !this.editMode;
  }

  buildForm() {
    this.form = this.fb.group({ name: [this.user.name, Validators.required] });
  }

  onSubmit() {
    /* istanbul ignore else */
    if (this.form.valid) {
      this.edit.emit({
        name: this.name.value,
        index: this.index
      });
    }
  }

  onDelete() {
    this.delete.emit(this.index);
  }
}
