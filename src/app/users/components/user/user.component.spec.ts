import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { UserComponent } from './user.component';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule ],
      declarations: [ UserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
  });

  it('should call buildForm() onInit', () => {
    const spy = spyOn(component, 'buildForm');

    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should build the form with user details passed down from parent', () => {
    component.user = { name: 'Nick' };
    component.buildForm();

    expect(component.name.value).toBe('Nick');
    expect(component.name.valid).toBe(true);
  });

  it('should toggle edit mode', () => {
    component.editMode = true;

    component.toggleMode();
    expect(component.editMode).toBe(false);

    component.toggleMode();
    expect(component.editMode).toBe(true);
  });

  it('should emit index on delete', () => {
    component.index = 4;
    const spy = spyOn(component.delete, 'emit');

    component.onDelete();

    expect(spy).toHaveBeenCalledWith(4);
  });

  it('should emit user on valid form submit', () => {
    component.user = { name: 'Nick' };
    component.index = 4;
    const spy = spyOn(component.edit, 'emit');

    component.buildForm();
    component.name.setValue('Tom');
    component.onSubmit();

    expect(spy).toHaveBeenCalledWith({
      name: 'Tom',
      index: 4
    });
  });
});
