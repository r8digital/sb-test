import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  @Output() addUser = new EventEmitter();

  form: FormGroup;

  get name() { return this.form.get('name'); }

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this.fb.group({ name: ['', Validators.required] });
  }

  onSubmit() {
    if (this.form.valid) {
      this.addUser.emit(this.form.value);
      this.form.reset();
    } else {
      this.validateControls(this.form);
    }
  }

  validateControls(form: FormGroup) {
    Object.keys(form.controls).forEach(controlName => {
      const control: AbstractControl = form.get(controlName);
      control.markAsTouched({ onlySelf: true });
      control.updateValueAndValidity({ onlySelf: true });
    });
  }
}
