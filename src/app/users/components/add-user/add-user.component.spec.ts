import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { AddUserComponent } from './add-user.component';

describe('AddUserComponent', () => {
  let component: AddUserComponent;
  let fixture: ComponentFixture<AddUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [ AddUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call buildForm() onInit', () => {
    const spy = spyOn(component, 'buildForm');

    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should build the form with empty text input', () => {
    component.buildForm();

    expect(component.name.value).toBe('');
    expect(component.name.valid).toBe(false);
  });

  it('should emit a valid form', () => {
    const spy = spyOn(component.addUser, 'emit');
    component.name.setValue('Peter');

    component.onSubmit();

    expect(spy).toHaveBeenCalledWith({ name: 'Peter' });
  });

  it('should mark controls as touched on invalid form submission', () => {
    const spy = spyOn(component, 'validateControls');
    component.name.setValue('');

    component.onSubmit();

    expect(spy).toHaveBeenCalled();
  });

  it('should validate form controls and mark as touched', () => {
    component.buildForm();

    component.validateControls(component.form);

    expect(component.name.touched).toBe(true);
  });
});
