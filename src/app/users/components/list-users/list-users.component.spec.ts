import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { ListUsersComponent } from './list-users.component';
import { UserComponent } from '../user/user.component';

describe('ListUsersComponent', () => {
  let component: ListUsersComponent;
  let fixture: ComponentFixture<ListUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule
      ],
      declarations: [
        ListUsersComponent,
        UserComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should emit index on delete', () => {
    const spy = spyOn(component.delete, 'emit');

    component.onDelete(3);

    expect(spy).toHaveBeenCalledWith(3);
  });

  it('should emit user on edit', () => {
    const user = {
      name: 'Tom',
      index: 7
    };
    const spy = spyOn(component.edit, 'emit');

    component.onEdit(user);

    expect(spy).toHaveBeenCalledWith(user);
  });
});
