import { Component, Input, Output, EventEmitter } from '@angular/core';

import { User } from '../../models/user.interface';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent {
  @Input() users: User[];
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();

  onEdit(user: User) {
    this.edit.emit(user);
  }

  onDelete(index: number) {
    this.delete.emit(index);
  }
}
