import {
  selectUsers,
  selectUsersLoaded,
  selectUsersLoading,
  selectUsersFailure
} from './selectors';

describe('UsersSelectors', () => {
  const initialState = {
    users: [
      { name: 'Nick' },
      { name: 'Tim' },
    ],
    loaded: true,
    loading: false,
    failure: false
  };

  it('should return users', () => {
    const result = selectUsers.projector(initialState);
    expect(result).toEqual(initialState.users);
  });

  it('should return loaded', () => {
    const result = selectUsersLoaded.projector(initialState);
    expect(result).toEqual(initialState.loaded);
  });

  it('should return loading', () => {
    const result = selectUsersLoading.projector(initialState);
    expect(result).toEqual(initialState.loading);
  });

  it('should return failure', () => {
    const result = selectUsersFailure.projector(initialState);
    expect(result).toEqual(initialState.failure);
  });

  it('should handle undefined', () => {
    expect(selectUsers.projector(undefined)).toEqual([]);
    expect(selectUsersLoaded.projector(undefined)).toEqual(false);
    expect(selectUsersLoading.projector(undefined)).toEqual(false);
    expect(selectUsersFailure.projector(undefined)).toEqual(false);
  });
});
