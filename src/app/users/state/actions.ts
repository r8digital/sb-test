import { Action } from '@ngrx/store';
import { User } from '../models/user.interface';
import * as constants from '../constants';

export class LoadUsersAction implements Action {
  readonly type = constants.api.LOAD_USERS;
}

export class LoadUsersSuccessAction implements Action {
  readonly type = constants.api.LOAD_USERS_SUCCESS;
  constructor(public payload: User[]) {}
}

export class LoadUsersFailAction implements Action {
  readonly type = constants.api.LOAD_USERS_FAILURE;
  constructor(public payload: any) {}
}

export class EditUserAction implements Action {
  readonly type = constants.api.EDIT_USER;
  constructor(public payload: User) {}
}

export class EditAllUsersAction implements Action {
  readonly type = constants.api.EDIT_USERS;
  constructor(public payload: User[]) {}
}

export class DeleteUserAction implements Action {
  readonly type = constants.api.DELETE_USER;
  constructor(public payload: number) {}
}

export class DeleteAllUsersAction implements Action {
  readonly type = constants.api.DELETE_USERS;
}

export class AddUserAction implements Action {
  readonly type = constants.api.ADD_USER;
  constructor(public payload: User) {}
}

export type UserActions = LoadUsersAction
  | LoadUsersSuccessAction
  | LoadUsersFailAction
  | EditUserAction
  | EditAllUsersAction
  | DeleteUserAction
  | DeleteAllUsersAction
  | AddUserAction;
