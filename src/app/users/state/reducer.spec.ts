import {
  LoadUsersAction,
  LoadUsersSuccessAction,
  LoadUsersFailAction,
  EditUserAction,
  EditAllUsersAction,
  DeleteUserAction,
  DeleteAllUsersAction,
  AddUserAction
} from './actions';
import { initialState, reducer } from './reducer';

describe('UsersReducer', () => {
  it('should return initial state', () => {
    const action = {};
    const state = reducer(initialState, action);

    expect(state).toEqual(initialState);
  });

  it('should set users loading states', () => {
    const action = new LoadUsersAction();
    const state = reducer(initialState, action);

    expect(state.loading).toBe(true);
    expect(state.loaded).toBe(false);
    expect(state.users).toEqual(initialState.users);
  });

  it('should handle users load success', () => {
    const users = [
      { name: 'Nick' },
      { name: 'Tim' },
    ];
    const action = new LoadUsersSuccessAction(users);
    const state = reducer(initialState, action);

    expect(state.loading).toBe(false);
    expect(state.loaded).toBe(true);
    expect(state.users).toEqual(users);
  });

  it('should handle users load failure', () => {
    const action = new LoadUsersFailAction({});
    const state = reducer(initialState, action);

    expect(state.loading).toBe(false);
    expect(state.loaded).toBe(false);
    expect(state.failure).toBe(true);
  });

  it('should handle edit user', () => {
    const user = { name: 'Nick', index: 0 };
    const action = new EditUserAction(user);
    const state = reducer(initialState, action);

    expect(state.users[0].name).toEqual('Nick');
  });

  it('should handle delete user', () => {
    const state = {
      users: [
        { name: 'Nick' },
        { name: 'Tim' },
      ],
      loaded: true,
      loading: false,
      failure: false
    };
    const action = new DeleteUserAction(1);
    const result = reducer(state, action);

    expect(result.users.length).toEqual(1);
  });

  it('should handle add user', () => {
    const state = {
      users: [
        { name: 'Nick' },
        { name: 'Tim' },
      ],
      loaded: true,
      loading: false,
      failure: false
    };
    const action = new AddUserAction({ name: 'Peter' });
    const result = reducer(state, action);

    expect(result.users.length).toEqual(3);
  });
});
