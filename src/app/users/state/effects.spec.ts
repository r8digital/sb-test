import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing/';
import { Observable, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';

import { LoadUsersAction, LoadUsersSuccessAction, LoadUsersFailAction } from './actions';
import { UsersEffects } from './effects';
import { UserServiceService } from '../services/user-service.service';

describe('UserEffects', () => {
  let actions: Observable<any>;
  let userService: jasmine.SpyObj<UserServiceService>;
  let effects: UsersEffects;

  beforeEach((() => {
    TestBed.configureTestingModule({
      providers: [
        UsersEffects, provideMockActions(() => actions),
        {
          provide: UserServiceService,
          useValue: {
            getUsers: jasmine.createSpy()
          }
        }
      ]
    });

    userService = TestBed.get(UserServiceService);
    effects = TestBed.get(UsersEffects);
  }));

  it('should return load users success action', () => {
    const users = [{ name: 'Nick' }];
    const action = new LoadUsersAction();
    const outcome = new LoadUsersSuccessAction(users);

    actions = hot('-a', { a: action });
    const response = cold('-a|', { a: users });
    userService.getUsers.and.returnValue(response);

    const expected = cold('--b', { b: outcome });
    expect(effects.loadUsers$).toBeObservable(expected);
  });
});
