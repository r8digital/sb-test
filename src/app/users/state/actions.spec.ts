import {
  LoadUsersAction,
  LoadUsersSuccessAction,
  LoadUsersFailAction,
  EditUserAction,
  EditAllUsersAction,
  DeleteUserAction,
  DeleteAllUsersAction,
  AddUserAction
} from './actions';
import { api as constants } from '../constants';

describe('UserActions', () => {
  it('should create LoadUsersAction', () => {
    const action = new LoadUsersAction();

    expect({ ...action }).toEqual({
      type: constants.LOAD_USERS
    });
  });

  it('should create LoadUsersSuccessAction', () => {
    const payload = [
      { name: 'Nick' }
    ];
    const action = new LoadUsersSuccessAction(payload);

    expect({ ...action }).toEqual({
      type: constants.LOAD_USERS_SUCCESS,
      payload
    });
  });

  it('should create LoadUsersFailAction', () => {
    const payload = [
      { error: '429 - too many requests'}
    ];
    const action = new LoadUsersFailAction(payload);

    expect({ ...action }).toEqual({
      type: constants.LOAD_USERS_FAILURE,
      payload
    });
  });

  it('should create EditUserAction', () => {
    const payload = { name: 'Nick', index: 6 };
    const action = new EditUserAction(payload);

    expect({ ...action }).toEqual({
      type: constants.EDIT_USER,
      payload
    });
  });

  it('should create EditAllUsersAction', () => {
    const payload = [{ name: 'Nick', index: 6 }];
    const action = new EditAllUsersAction(payload);

    expect({ ...action }).toEqual({
      type: constants.EDIT_USERS,
      payload
    });
  });

  it('should create DeleteUserAction', () => {
    const payload = 3;
    const action = new DeleteUserAction(payload);

    expect({ ...action }).toEqual({
      type: constants.DELETE_USER,
      payload
    });
  });

  it('should create DeleteAllUsersAction', () => {
    const action = new DeleteAllUsersAction();

    expect({ ...action }).toEqual({
      type: constants.DELETE_USERS
    });
  });

  it('should create AddUserAction', () => {
    const payload = { name: 'Nick', index: 6 };
    const action = new AddUserAction(payload);

    expect({ ...action }).toEqual({
      type: constants.ADD_USER,
      payload
    });
  });
});
