import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectUsersState = createFeatureSelector<any>('users-module');
export const selectUsers = createSelector(selectUsersState, (state) => state ? state.users : []);
export const selectUsersLoaded = createSelector(selectUsersState, (state) => state ? state.loaded : false);
export const selectUsersLoading = createSelector(selectUsersState, (state) => state ? state.loading : false);
export const selectUsersFailure = createSelector(selectUsersState, (state) => state ? state.failure : false);
