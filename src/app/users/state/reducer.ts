import { User } from '../models/user.interface';
import { api as constants } from '../constants';

export interface UsersState {
  users: User[];
  loading: boolean;
  loaded: boolean;
  failure: boolean;
}

export const initialState: UsersState = {
  users: [],
  loading: false,
  loaded: false,
  failure: false
};

export function reducer(state = initialState, action: any): UsersState {
  switch (action.type) {
    case constants.LOAD_USERS: {
      return {
        ...state,
        loading: true
      };
    }
    case constants.LOAD_USERS_SUCCESS: {
      return {
        ...state,
        users: action.payload,
        loading: false,
        loaded: true
      };
    }
    case constants.LOAD_USERS_FAILURE: {
      return {
        ...state,
        loading: false,
        loaded: false,
        failure: true
      };
    }
    case constants.EDIT_USER: {
      const newUsers = state.users;
      newUsers[action.payload.index] = action.payload;
      return {
        ...state,
        users: newUsers
      };
    }
    case constants.DELETE_USER: {
      const newUsers = state.users;
      newUsers.splice(action.payload, 1);
      return {
        ...state,
        users: newUsers
      };
    }
    case constants.ADD_USER: {
      const newUsers = state.users;
      newUsers.push(action.payload);
      return {
        ...state,
        users: newUsers
      };
    }
    default:
      return state;
  }
}
