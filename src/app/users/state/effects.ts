import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { UserServiceService } from '../services/user-service.service';
import { LoadUsersSuccessAction, LoadUsersFailAction } from './actions';
import * as constants from '../constants';

@Injectable()
export class UsersEffects {
  constructor(
    private actions$: Actions,
    private userService: UserServiceService
  ) {}

  @Effect()
  loadUsers$ = this.actions$.pipe(
    ofType(constants.api.LOAD_USERS),
    switchMap(() => {
      return this.userService.getUsers().pipe(
        map((data: any) =>  new LoadUsersSuccessAction(data)),
        catchError((error: any) => {
          return of(new LoadUsersFailAction(error));
        })
      );
    })
  );
}
