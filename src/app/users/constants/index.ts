export const api = {
  LOAD_USERS: '[USERS] Load users',
  LOAD_USERS_SUCCESS: '[USERS] Load users success',
  LOAD_USERS_FAILURE: '[USERS] Load users failure',
  EDIT_USER: '[USERS] Edit user',
  EDIT_USERS: '[USERS] Edit users',
  DELETE_USER: '[USERS] Delete user',
  DELETE_USERS: '[USERS] Delete users',
  ADD_USER: '[Users] Add user'
};
